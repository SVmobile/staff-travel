//
//  ContactUsViewController.h
//  StaffTravel
//
//  Created by Mohammed Alquzi on 6/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
///

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"


#define CONTACT_US @"Contact Us"

@interface ContactUsViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (IBAction)CallFlightBookingDept:(id)sender;
- (IBAction)CallHelpDesk:(id)sender;
- (IBAction)EmailMobileAppTeamButton:(id)sender;


@end
