//
//  LoginBL.h
//  StaffTravel
//
//  Created by Ali Mansho on 9/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginBL : NSObject
- (void) writeToPlist:(NSString *) userID userPassword:(NSString *) password payrollNumber:(NSString *)prn staffName:(NSString *) userName userGroups:(NSString *) groups error:(NSString *) errorMessage;
+ (NSMutableDictionary *) readFromPlist;
+ (void)removeDataFromPlist;

@end
