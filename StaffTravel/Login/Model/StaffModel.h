//
//  StaffModel.h
//  eBusinssLogin
//
//  Created by Mohammad Bahadur on 6/9/14.
//  Copyright (c) 2014 Mohammad Bahadur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StaffModel : NSObject
@property (copy,nonatomic) NSString *prn;
@property (copy,nonatomic) NSString *name;
@property (copy,nonatomic) NSString *groups;
@property (copy,nonatomic) NSString *error;

@end
