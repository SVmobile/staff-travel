//
//  LogingUIViewController.m
//  SaudiaStaffTravel
//
//  Created by Ali Mansho on 4/13/14.
//  Copyright (c) 2014 Ali Mansho. All rights reserved.
//

#import "LoginUIViewController.h"
#import "MainMenuViewController.h"
#import "HockeySDK/HockeySDK.h"


@interface LoginUIViewController ()

@end

@implementation LoginUIViewController

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // application.applicationSupportsShakeToEdit = YES;
    return YES;
}
//>


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:LOGIN];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidLoad
{
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    [Utility isInternetReachable];
    

    self.passwordTextField.delegate = self;
    self.userIDTextField.delegate = self;
    self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.userIDTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [super viewDidLoad];
    
    //I disabled it through the GUI, but the following line does disable it as well...
    //self.detailedInfoTextView.scrollEnabled = NO;
    
    self.myWeb.delegate = self;
    
    
    //    if (!(self.passwordTextField.text.length && self.userIDTextField.text.length))
    //    {
    //        self.loginButtonOutlet.enabled = NO;
    //        self.loginButtonOutlet.alpha =.5;
    //    }
    // Do any additional setup after loading the view.
    
    //Check internet connection
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)youtubeBtnAction:(id)sender {
    //    UIButton *btn = (UIButton *)sender;
    //
    //    if( [[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"youtube+.png"]])
    //    {
    //        [btn setImage:[UIImage imageNamed:@"ygray.png"] forState:UIControlStateNormal];
    //        // other statements
    //    }
    //    else
    //    {
    //        [btn setImage:[UIImage imageNamed:@"youtube+.png"] forState:UIControlStateNormal];
    //        // other statements
    //    }
    
    //self.userIDTextField.text = @"PAX27";
    // self.passwordTextField.text = @"";
}


//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSInteger MAXLENGTH = 12;
//
//    NSUInteger newLength = [textField.text length] - range.length + [string length];
//    if (newLength >= MAXLENGTH) {
//        textField.text = [[textField.text stringByReplacingCharactersInRange:range withString:string] substringToIndex:MAXLENGTH];
//        return NO;
//    }
//
//
//    if (self.userIDTextField.text.length == 0 || self.passwordTextField.text.length == 0)
//    {
//        NSLog(@" both are 0 size...");
//        NSString *testString = [self.userIDTextField.text stringByReplacingCharactersInRange:range withString:string];
//
//        if (!(testString.length))
//        {
//            self.loginButtonOutlet.alpha = .5;
//            self.loginButtonOutlet.enabled = NO;;
//        }
//
////        else if(testString.length && (testString2.length))
////        {
////            self.loginButtonOutlet.alpha = 1;
////            self.loginButtonOutlet.enabled = YES;
////        }
//    }
//
//    else if (self.userIDTextField.text.length == 0)
//    {
//        NSLog(@" password only is 0 size...");
//
//
//        NSString *testString3 = [self.userIDTextField.text stringByReplacingCharactersInRange:range withString:string];
//
//        if (!testString3.length)
//        {
//            self.loginButtonOutlet.alpha = .5;
//            self.loginButtonOutlet.enabled = NO;;
//        }
//
//
//    }
//
//    else if (self.passwordTextField.text.length == 0)
//    {
//        NSLog(@" password only is 0 size...");
//
//
//        NSString *testString = [self.passwordTextField.text stringByReplacingCharactersInRange:range withString:string];
//
//        if (!testString.length)
//        {
//            self.loginButtonOutlet.alpha = .5;
//            self.loginButtonOutlet.enabled = NO;;
//        }
//
////        else if(testString.length && (testString2.length))
////        {
////            self.loginButtonOutlet.alpha = 1;
////            self.loginButtonOutlet.enabled = YES;
////        }
//    }
//
//   else
//   {
//       NSLog(@" bonth are > 0 size - else statement...");
//
//           self.loginButtonOutlet.alpha = 1;
//       self.loginButtonOutlet.enabled = YES;
//   }
//
//
//
////   NSString *userIDString = [self.userIDTextField.text stringByReplacingCharactersInRange:range withString:string];
////
////    NSString *passwordString = [self.passwordTextField.text  stringByReplacingCharactersInRange:range withString:string];
//
//    return YES;
//}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //    if ((self.userIDTextField.text.length == 0) || (self.passwordTextField.text.length ==0)) {
    //        self.loginButtonOutlet.alpha = .5;
    //        self.loginButtonOutlet.enabled = NO;
    //    }
    
    //else
    
    
    
    //The follwoing code is to allow the return key "Next" go to the next textfield (Ali Mansho).
    if (textField == self.userIDTextField)
    {
        [self.passwordTextField becomeFirstResponder];
    }
    
    else if([Utility isInternetReachable])
    {
        if (textField == self.passwordTextField)
        {
            // A.M.
            // Test the internet connection and alert if there is no connection (Reachability)....
            
            if((self.userIDTextField.text.length) > 0 && (self.passwordTextField.text.length > 0))
            {
                [self.view endEditing:YES];
                NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                                 @"username", self.userIDTextField.text,
                                                 @"password", self.passwordTextField.text,
                                                 nil];
                [self UIWebViewWithPost:self.myWeb url:@"https://portal.saudiairlines.com.sa/pkmslogin.form" params:webViewParams];
                //[SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
                [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];
            }
            else
            {
                // One or both fields are empty by A.M.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter User ID and Password." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }
    return YES;
}

#pragma Authentication

- (void)UIWebViewWithPost:(UIWebView *)uiWebView url:(NSString *)url params:(NSMutableArray *)params
{
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", url]];
    if([params count] % 2 == 1) { NSLog(@"UIWebViewWithPost error: params don't seem right"); return; }
    
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >\n", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    
    [s appendString: @"</input></form></body></html>"];
    //NSLog(@"%@", s);
    [uiWebView loadHTMLString:s baseURL:nil];
}



- (IBAction)loginButton:(id)sender {
    
    if([Utility isInternetReachable])
    {
        if((self.userIDTextField.text.length) > 0 && (self.passwordTextField.text.length > 0))
        {
            [self.view endEditing:YES];
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"username", self.userIDTextField.text,
                                             @"password", self.passwordTextField.text,
                                             nil];
            [self UIWebViewWithPost:self.myWeb url:@"https://portal.saudiairlines.com.sa/pkmslogin.form" params:webViewParams];
            // [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
            
            [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];
        }
        else
        {
            // One or both fields are empty by A.M.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter User ID and Password." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *currentURL;
    NSString *yourHTMLSourceCodeString = @"";
    GLobal *global = [GLobal sharedManager];
    yourHTMLSourceCodeString = [self.myWeb stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
    currentURL = self.myWeb.request.URL.absoluteString;
    
    // NSLog(@"Mansho123 Mansho Mansho ");
    //NSLog(yourHTMLSourceCodeString);
    //*************** End of code to get the HTML ************************
    
    NSLog(@"title:%@",currentURL);
    
    // This is to control expired passwords and dismiss the loading...
    if ([yourHTMLSourceCodeString rangeOfString:@"password has expired"].location != NSNotFound)
    {
        [self.myWeb stopLoading];
        //Call deleteCookies function to delete the cookies
        [Utility deleteCookies];
        currentURL = @"";
        self.passwordTextField.text = @"";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your password has expired, please change it through eBusiness Center and try again." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    
    else if ([yourHTMLSourceCodeString rangeOfString:@"Login failed"].location != NSNotFound)
        //([currentURL isEqualToString:@"https://portal.saudiairlines.com.sa/pkmslogin.form"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your User ID or Password is incorrect." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    
    else  if ([currentURL isEqualToString:@"https://portal.saudiairlines.com.sa/portal/SV/mainPage.jsp"] || ([currentURL isEqualToString:@"https://portal.saudiairlines.com.sa/portal/SV/loginsuccess.jsp"] && [yourHTMLSourceCodeString rangeOfString:@"Login failed"].location == NSNotFound))
    {
        currentURL =@"";
//        NSLog(@"Testing - Mansho 9990");
//        NSLog(yourHTMLSourceCodeString);
        //[SVProgressHUD dismiss];
        NSURL *url = [NSURL URLWithString: @"https://portal.saudiairlines.com.sa/portal/SV/MobileApp.jsp"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL: url];
        [self.myWeb loadRequest: request];
        
    }
    
    else if ([currentURL isEqualToString:@"https://portal.saudiairlines.com.sa/portal/SV/MobileApp.jsp"])
    {
       
      MainMenuViewController *mainMenuViewController = [[MainMenuViewController alloc]initWithNibName:@"MainMenuViewController" bundle:nil];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainMenuViewController];

        NSError *error;
        [SVProgressHUD dismiss];
        NSLog(@"JSON Received....");
        
        NSString *jsonString = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        NSString *jsonModified = [jsonString stringByReplacingOccurrencesOfString:@"<html><head></head><body>" withString:@""];
        jsonModified = [jsonModified stringByReplacingOccurrencesOfString:@"</body></html>" withString:@""];
        
        NSData *jsonData = [jsonModified dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* json =[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        //NSDictionary* json =[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"JSON:%@", jsonModified);
        NSDictionary* emp = [json objectForKey:@"employee"];
        self.staff   =   [[StaffModel alloc]init];
        self.staff.prn = [emp objectForKey:@"prn"];
        self.staff.name = [emp objectForKey:@"name"];
        self.staff.groups = [emp objectForKey:@"gourp"];
        self.staff.error = [emp objectForKey:@"error"];
        mainMenuViewController.staff = self.staff;
        global.staff = self.staff;
        
    //    NSLog(@"prn is By Mansho:  %@", self.staff.prn);
        // Saving the data into plist in order to avoid login the user again....
        LoginBL *utilityObj = [[LoginBL alloc] init];
        [utilityObj writeToPlist:self.userIDTextField.text userPassword:self.passwordTextField.text payrollNumber:self.staff.prn staffName:self.staff.name userGroups:self.staff.groups error:self.staff.error];
        
        navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        //navigationController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:navigationController animated:YES completion:^{}];

        NSLog(@"Staff Name:%@", self.staff.name);
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{[self.view endEditing:YES];}

//Hide Navigation Bar
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.passwordTextField.text = @"";
    [[BITHockeyManager sharedHockeyManager].updateManager checkForUpdate];//To inform the user in case there is an update....
}


- (IBAction)twitterButton:(id)sender
{
    SavingPnrViewController *savingPnrViewController = [[SavingPnrViewController alloc] initWithNibName:@"SavingPnrViewController" bundle:nil];
    [self.navigationController pushViewController:savingPnrViewController animated:YES];
    NSLog(@"SavingPnrViewController");
}

- (IBAction)detailedInfoButton:(id)sender
{
    if (self.detailedInfoTextView.hidden == YES)
    {
        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
        NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
        
        NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
        NSString *build = infoDictionary[(NSString*)kCFBundleVersionKey];
        NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
        NSString *systemName = [[UIDevice currentDevice] systemName];
        
        /* Show other info.:
         
         //NSString *bundleName = infoDictionary[(NSString *)kCFBundleNameKey];
         //    NSString *model = [[UIDevice currentDevice] model];
         //    NSString *iPhoneName = [[UIDevice currentDevice] name];
         
         */
        //user name & password...
        
            //  self.userIDTextField.text = @"PAX27";
             //self.passwordTextField.text = @"";
        
        self.detailedInfoTextView.hidden = NO;
        
        //  self.detailedInfoTextView.text = [ NSString stringWithFormat:@"\n Version: %@ Build: %@ \n System name: %@ \n OS: %@", version, build, systemName, systemVersion ];
        
        self.detailedInfoTextView.text = [ NSString stringWithFormat:@"\n Version: %@ Build: %@ Year: 2014\n Lead by: Khalid Alumari \n Developers: Ali M. Mansho - Mohammed Bahadur - Mohammed Alquzi", version, build];
        
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"
                                                              action:@"touch"
                                                               label:LOGIN_INFO_BUTTON
                                                               value:nil] build]];
        [tracker set:kGAIScreenName value:nil];
    }
    
    else
    {
        self.detailedInfoTextView.hidden = YES;
    }
}

- (IBAction)yammerButton:(id)sender
{
    //    NSString *wazeAppURL = @"waze://";
    //    NSString *mapsAppURL = @"maps://";
    //    NSString *facebookAppURL = @"fb://"; // To open facebook app....
    
    //The folloiwng is to open Yammer through Native yammer app if it's installed already on your iPhone.
    NSString *yammerAppURL = @"Yammer://";
    
    //The following is to open Yammer through Safari - Website...
    NSString *mapsAppURL = @"http://yammer.com";
    
    //The following is to open the Yammer through iTunes incase is not existed to download it.
    //    NSString *mapsAppURL = @"http://itunes.com/app/Yammer";
    
    
    BOOL canOpenURL = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:yammerAppURL]];
    
    NSString *url = canOpenURL ? yammerAppURL : mapsAppURL;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

//This method is to cancel the laoding when a user taps on it by Ali Mansho (A.M.)
- (void)hudTapped:(NSNotification *)notification
{
    [self.myWeb stopLoading];
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}

@end