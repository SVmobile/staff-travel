//
//  LogingUIViewController.h
//  SaudiaStaffTravel
//
//  Created by Ali Mansho on 4/13/14.
//  Copyright (c) 2014 Ali Mansho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMenuViewController.h"
#import "SVProgressHUD.h"
#import "StaffModel.h"
#import "GLobal.h"
#import "SavingPnrViewController.h"
#import "LoginBL.h"

#define LOGIN @"LOGIN"
#define LOGIN_INFO_BUTTON @"Login Info Button"

@interface LoginUIViewController : UIViewController <UITextFieldDelegate, NSURLConnectionDelegate, UIWebViewDelegate, UINavigationControllerDelegate>

@property(strong,nonatomic) StaffModel* staff;
@property (strong, nonatomic) IBOutlet UIButton *youtubeBtnOutlet;
- (IBAction)youtubeBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *userIDTextField;
@property (strong, nonatomic) IBOutlet UIWebView *myWeb; 

- (IBAction)loginButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *loginButtonOutlet;
- (IBAction)twitterButton:(id)sender;
- (IBAction)detailedInfoButton:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *detailedInfoTextView;
@property (strong, nonatomic) IBOutlet UIButton *yammerBtnOutlet;
- (IBAction)yammerButton:(id)sender;

@end
