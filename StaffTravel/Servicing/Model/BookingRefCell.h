//
//  BookingRefCell.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 4/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingRefCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *bookingRefText;

@end
