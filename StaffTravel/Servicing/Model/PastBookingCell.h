//
//  PastBookingCell.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 5/5/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastBookingCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *bookingRefLabel;
@property (strong, nonatomic) IBOutlet UILabel *fromLabel;
@property (strong, nonatomic) IBOutlet UILabel *toLabel;
@property (strong, nonatomic) IBOutlet UILabel *dashLabel;

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@end
