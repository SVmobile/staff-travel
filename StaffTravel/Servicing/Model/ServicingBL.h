//
//  ServicingBL.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 6/29/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Booking.h"
#import "BookingItinerary.h"
#import "MessageResponse.h"

#define PNR_RETRIEVE_PNR_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobilePnrSrvServlet"

@interface ServicingBL : NSObject


- (Booking*) retrieveBookingFromJsonOfString:(NSString*)jsonString;
- (MessageResponse*) getMessageResponseFromJsonOfString:(NSString*)jsonString;
- (NSString*) getRefNumberInItineraries:(NSArray*)itineraries;
- (BOOL) isAllSegmentConfirmedInItineraries:(NSArray*)Itineraries;
@end
