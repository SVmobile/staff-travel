//
//  ServicingTableViewController.m
//  StaffTravel
//
//  Created by Mohammad Bahadur on 4/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "ServicingTableViewController.h"

#define PNR_RETRIEVE_PNR_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobilePnrSrvServlet"


@interface ServicingTableViewController ()

@end

@implementation ServicingTableViewController

#pragma mark - RecessionTipsParserDeleagate Methods Implementation

-(void) didReceiveBookingRetrieve:(Booking*) bookingRetrieve
{
    booking    =   bookingRetrieve;
}

-(id) init
{
    if (self) {
        
        itn = [[BookingItinerary alloc]init];
        
       // NSLog(@"init");
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SERVICING_INPUT];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidLoad
{
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    [Utility isInternetReachable];
    
    GLobal *global = [GLobal sharedManager];
    
    
    //To Color the Navigation bar for all the views, but it's been moved to the menu view controller....
    //    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:138.0/255.0 green:129.0/255.0 blue:100.0/255.0 alpha:1.0]];
    //    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Bangla Sangam MN" size:17.0]}];
    //    [self.navigationController.navigationBar setTintColor:[UIColor grayColor]];
    
    //[self.navigationController.navigationBar setTitleTextAttributes:@{}];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = YES;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skyBgAlpha.png"]];
    
    
    self.bookingRefText.delegate = self;
    if (!self.bookingRefText.text.length)
    {
        self.displayBookingOutletButton.enabled = NO;
        self.displayBookingOutletButton.alpha = .5;
    }
    self.bookingRefText.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [super viewDidLoad];
    
    [self.navigationController setTitle:@"Booking Services"];
    
    self.webView.delegate = self;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    //[self.navigationController setTitle:@"Servicing"];
    self.navigationItem.title = @"Servicing";
    // [super viewDidLoad];
    
    //NSLog(@"Global staff value: %@", global.staff.name);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    ////////
    
    PnrDatadb *tempdb = [PnrDatadb sharedInstance];
    pnrs = [tempdb selectFromPnrDataWithPrn:global.staff.prn];
    if ([pnrs count] == 0)
    {
        NSLog(@"No data found in the database...");
        /* UIAlertView *noDataAlert = [[UIAlertView alloc] initWithTitle:@"There is no data to show." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [noDataAlert show];*/
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    int rows = 0;
    
    if (section == 0) {
        rows = 1;
    }
    else if(section == 1)
    {
        rows = pnrs.count;
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //float height = 45;
    float height = 60;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            height = 128;
        } else if(indexPath.row == 1)
        {
            //float height = 65;
        }
    }
    return height;
}
//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 60)];
//
//    [headerView setBackgroundColor:[UIColor redColor]];
//    return headerView;
//}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* title = [[NSString alloc]init];
    
    switch(section)
    {
        case 1:
            title = @"Bookings History";
            break;
    }
    
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"servicingCell1";
    UITableViewCell *curCell = [[UITableViewCell alloc]init] ;
    if (indexPath.section == 0) {
        
        //  tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //PastBookingCell* pastBookingCell = [[PastBooking alloc]ini];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if (indexPath.row == 0) {
            curCell = self.bookingRefCell;
        }
        else if(indexPath.row == 1)
        {
            curCell = self.displayBookingRefCell;
        }
    }
    
    else
    {
        //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        static NSString *CellIdentifier = @"servicingCell2";
        
        curCell = (PastBookingCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (curCell == nil)
        {
            // NSLog(@"New Cell Made");
            
            NSMutableArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PastBookingCell" owner:self options:nil];
            
            for(id currentObject in topLevelObjects)
            {
                if([currentObject isKindOfClass:[PastBookingCell class]])
                {
                    curCell = (PastBookingCell *)currentObject;
                    break;
                }
            }
        }
        
        ((PastBookingCell *)curCell).bookingRefLabel.text = ((PnrData*)[pnrs objectAtIndex:indexPath.row]).pnrC;
        
       //   NSLog(@" Length of Origin Before: %lu", (unsigned long)((PnrData*)[pnrs objectAtIndex:indexPath.row]).originC.length);
        
        if(((PnrData*)[pnrs objectAtIndex:indexPath.row]).originC.length)
        {
            ((PastBookingCell *)curCell).dateLabel.text = ((PnrData*)[pnrs objectAtIndex:indexPath.row]).flight_dateC;
            
            ((PastBookingCell *)curCell).fromLabel.text = ((PnrData*)[pnrs objectAtIndex:indexPath.row]).originC;
            
            ((PastBookingCell *)curCell).dashLabel.text =  @" - ";
            
            ((PastBookingCell *)curCell).toLabel.text =    ((PnrData*)[pnrs objectAtIndex:indexPath.row]).destinationC;
        }
        
    }
    
    return curCell;
}



- (void)UIWebViewWithPost:(UIWebView *)uiWebView url:(NSString *)url params:(NSMutableArray *)params
{
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", url]];
    if([params count] % 2 == 1) { NSLog(@"UIWebViewWithPost error: params don't seem right"); return; }
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >\n", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    [s appendString: @"</input></form></body></html>"];
    NSLog(@"r %@", s);
    [uiWebView loadHTMLString:s baseURL:nil];
}



- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //[SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
    
    [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    ServicingBL *servicingBl = [[ServicingBL alloc]init];
    PnrDatadb *tempdb = [PnrDatadb sharedInstance];
    NSString *currentURL = self.webView.request.URL.absoluteString;
    GLobal *global = [GLobal sharedManager];
    
    NSLog(@"title:%@",currentURL);
    
    if ([currentURL isEqualToString:@PNR_RETRIEVE_PNR_URL])
    {
        NSString *jsonString = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        booking = [servicingBl retrieveBookingFromJsonOfString:jsonString];
        
#pragma status succeeded
        if ([booking.status isEqualToString:@"s"])
        {
            //The following check is to make sure no NULL data is existed - replacing the NULL data with empty string
            if (booking.itineraries.count == 0)
            {
                NSLog(@"Itineraries.count is ZERO");
                itn.depDate = @"";
                itn.origin = @"";
                itn.dest = @"";
            }
            
            else
            {
                itn = [booking.itineraries objectAtIndex:0];
            }
            
            // To check if we have the PNR saved in our database or not... If not we have to insert its data.
            NSMutableArray *interalPnrs = [tempdb selectFromPnrDataWithPnr:booking.alphanumeric];
            if([interalPnrs count] != 0)
            {
                NSLog(@"PRN: %@", global.staff.prn);
                
                NSLog(@"It is an existed PNR.......");
                // It is updating the PNR information because the PNR is already existed (Ali M. Mansho).
                
                tempdb = [PnrDatadb sharedInstance];
                [tempdb updatePnr:booking.alphanumeric prn:global.staff.prn flightDate:itn.depDate origin:itn.origin destination:itn.dest json:jsonString];
            }
            
            else
            {
                NSLog(@"It's about to insert the data...");
                [tempdb insertIntoPnr:booking.alphanumeric prn:global.staff.prn flightDate:itn.depDate origin:itn.origin destination:itn.dest json:jsonString];
                
                //The following line is to load the view witht the new PNR so when the user clicks on back button, the table will be updated with the new PNR.
                pnrs = [tempdb selectFromPnrDataWithPrn:global.staff.prn];
                
                [self.tableView reloadData];
                // NSLog(@"numeric:%@", [booking.names objectAtIndex:0]);
            }
            
            //The following code is to move the user to next screen wereas there is a PNR or not, there is Null data or not
            BookingDisplayTableViewController *bookingDisplayViewController = [[BookingDisplayTableViewController alloc]init];
            bookingDisplayViewController.booking = booking;
            [self.navigationController pushViewController:bookingDisplayViewController animated:YES];
            
            //By Ali Mansho....
        }
#pragma
        else
        {
            // NSLog(@"Mansho: %@", booking.message);
            if (booking.message == nil)
            {
                booking.message = @"Your reservation number is not found.";
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:booking.message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [SVProgressHUD dismiss];
    }
}

#pragma mark IBActions
- (IBAction)displayBooking:(id)sender {
    
    if ([Utility isInternetReachable]) {
        
        if (self.bookingRefText.text.length == 0) {
            //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a PNR" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //        [alert show];
            
            self.displayBookingOutletButton.alpha = .5;
            self.displayBookingOutletButton.enabled = NO;
        }
        else
        {
            [self.view endEditing:YES];
            GLobal *global = [GLobal sharedManager];
            
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"userPRN", global.staff.prn,
                                             @"pnr", self.bookingRefText.text,
                                             nil];
            
            [self UIWebViewWithPost:self.webView url:@PNR_RETRIEVE_PNR_URL params:webViewParams];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [self.bookingRefText.text length] + [string length] - range.length;
    
    NSString *testString = [self.bookingRefText.text stringByReplacingCharactersInRange:range withString:string];
    
    if(testString.length)
    {
        // NSLog(@"If statement - Length = %lu", (unsigned long)testString.length);
        self.displayBookingOutletButton.alpha = 1;
        self.displayBookingOutletButton.enabled = YES;
    }
    else if (!testString.length)
    {
        //     NSLog(@"elese statment - Length = %lu", (unsigned long)testString.length);
        self.displayBookingOutletButton.alpha = .5;
        self.displayBookingOutletButton.enabled = NO;
    }
    
    return (newLength <= BOOKINGREFERENCE_LIMIT);
}

- (BOOL) textFieldShouldClear:(UITextField *)textField
{
    //  NSLog(@"User has just clicked on Clear Button....");
    self.displayBookingOutletButton.enabled = NO;
    self.displayBookingOutletButton.alpha = .5;
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([Utility isInternetReachable]) {
        
        if (self.bookingRefText.text.length == 0) {
            self.displayBookingOutletButton.alpha = .5;
            self.displayBookingOutletButton.enabled = NO;
        }
        else
        {
            [self.view endEditing:YES];
            GLobal *global = [GLobal sharedManager];
            
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"userPRN", global.staff.prn,
                                             @"pnr", self.bookingRefText.text,
                                             nil];
            
            [self UIWebViewWithPost:self.webView url:@PNR_RETRIEVE_PNR_URL params:webViewParams];
        }
    }
    return YES;
}



// Hide Keyboard on scroll
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

//To allow the delete swipe


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // GLobal *global = [GLobal sharedManager];
    //PnrDatadb *tempdb = [PnrDatadb sharedInstance];
    //
    //pnrsSwipe = [tempdb selectFromPnrDataWithPrn:global.staff.prn];
    
    
    // if (indexPath.section == 0) {
    // editingStyle
    // self.servicingTableView.editing = NO;
    // }
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        
//        NSLog(@" Mansho - the PNR for the selected row....999 ====> %@ ", ((PnrData*)[pnrs objectAtIndex:indexPath.row]).pnrC);
//        
//        NSLog(@" Mansho - the PNR for the selected row....1010 ====> %ld ", (long)indexPath.row);
//        
//        NSLog(@"*****************************");
//        NSLog(@"Before delete");
//        for (PnrData *pnrData in pnrs) {
//            NSLog(@"PNR %@", pnrData.pnrC);
//        }
        
        PnrDatadb *tempdbDelete;
        tempdbDelete = [PnrDatadb sharedInstance];
        
        NSString *pnrToDelete = ((PnrData*)[pnrs objectAtIndex:indexPath.row]).pnrC;
        //delete the PNR from SQLite...
        [tempdbDelete deletePnr: pnrToDelete];
        
        //Remove the cell from the view when it's swiped...
        [pnrs removeObjectAtIndex:indexPath.row];
        
        /*  NSLog(@"*****************************");
         
         
         NSLog(@"After delete");
         for (PnrData *pnrData in pnrs) {
         NSLog(@"PNR %@", pnrData.pnrC);
         }
         NSLog(@"*****************************"); */
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        // [tableView reloadData];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear method");
    //The follwoing 2 lines are to reload tableview when clicked on back button from the next screen...
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    //    [self.servicingTableView reloadData];
    
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Detemine if it's in editing mode
//    if (self.tableView.editing)
//    {
//        NSLog(@"  Mansho ---> if (self.tableView.editing)...");
//        return UITableViewCellEditingStyleDelete;
//    }
//
//    return UITableViewCellEditingStyleNone;
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"You have selected this row by Mansho: %d", indexPath.row); // you can see selected row number in your console;
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    if([Utility isInternetReachable])
    {
        [self.view endEditing:YES];
        
        if (indexPath.section == 0)
        {
            NSLog(@"Section is 0 by AliMansho:");
        }
        
        else
        {
            
            GLobal *global = [GLobal sharedManager];
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"userPRN", global.staff.prn,
                                             @"pnr", ((PnrData*)[pnrs objectAtIndex:indexPath.row]).pnrC, nil];
            
            [self UIWebViewWithPost:self.webView url:@PNR_RETRIEVE_PNR_URL params:webViewParams];
            
        }
    }
    
}

//This method is to cancel the laoding when a user taps on it by Ali Mansho (A.M.)
- (void)hudTapped:(NSNotification *)notification
{
    [self.webView stopLoading];
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}

@end
