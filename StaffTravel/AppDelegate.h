//
//  AppDelegate.h
//  StaffTravel
//
//  Created by Mohammed Alquzi on 4/22/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServicingTableViewController.h"
#import "LoginUIViewController.h"
#import "SavingPnrViewController.h"
#import "FlightInfoTableViewController.h"
#import "LoginBL.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property(strong,nonatomic) StaffModel* staff;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL loginMainFlag;


@end
