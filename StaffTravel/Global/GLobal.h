//
//  GLobal.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 6/29/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StaffModel.h"

#define FLIGHT_INFO_INPUT @"Flight Info Input"

@interface GLobal : NSObject
{
    UIAlertView *errorView;
}
@property (nonatomic, strong) StaffModel *staff;

+ (id)sharedManager;
@end
