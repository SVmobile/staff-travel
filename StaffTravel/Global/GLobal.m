//
//  GLobal.m
//  StaffTravel
//
//  Created by Mohammad Bahadur on 6/29/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "GLobal.h"

@implementation GLobal

#pragma mark Singleton Methods

+ (id)sharedManager {
    static GLobal *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
    }    
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}
@end
