//
//  Utility.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 8/27/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface Utility : NSObject

+ (BOOL)isInternetReachable;
+ (void) deleteCookies;
@end
