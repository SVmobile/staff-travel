//
//  BookingDisplayTableViewController.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 5/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItenraryTableViewCell.h"
#import "PassengerTableViewCell.h"
#import "TicketTableViewCell.h"
#import "Booking.h"
#import "BookingItinerary.h"
#import "ServicingBL.h"
#import "GLobal.h"
#import "MessageResponse.h"
#import "Utility.h"
#import "SVProgressHUD.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"

#define BOOKING_DISPLAY @"Display Booking"
#define CONFIRM_BOOKING_BUTTON @"Confrim Booking Button"
#define ACTIVATE_SADAD_BUTTON @"Activate Sadad Button"
#define CANCEL_BOOKING_BUTTON @"Cancel Booking Button"

@interface BookingDisplayTableViewController : UITableViewController<UIWebViewDelegate>
{
    BookingItinerary* itn;
    MessageResponse* messageResponse;
    UIAlertView *cancelAlertView;
    UIAlertView *sadad27AlertView;
    UIAlertView *sadadConfirmationAlertView;
}
@property (strong, nonatomic) IBOutlet UITableViewCell* noTicketsCell;
@property (strong, nonatomic) IBOutlet UILabel * noTicketsLabel;

@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) Booking *booking;

@property (strong, nonatomic) IBOutlet UITableViewCell *timeLimitCell;
@property (strong, nonatomic) IBOutlet UILabel *timeLimitLabel;
@property (strong, nonatomic) IBOutlet UITableViewCell *BookingServicesCell;

@property (strong, nonatomic) IBOutlet UIButton *confirmBookingButton;
@property (strong, nonatomic) IBOutlet UIButton *activateSadadButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelBookingButton;

- (IBAction)confirmBooking:(id)sender;
- (IBAction)activateSadad:(id)sender;
- (IBAction)cancelBooking:(id)sender;
- (void)pnrRetrieve;
- (void)deactivateButtons;
- (void)hudTapped:(NSNotification *)notification;
@end
