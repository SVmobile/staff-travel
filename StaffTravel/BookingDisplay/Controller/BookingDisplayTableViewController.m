//
//  BookingDisplayTableViewController.m
//  StaffTravel
//
//  Created by Mohammad Bahadur on 5/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "BookingDisplayTableViewController.h"
#define ACTIVATE_SADDAD_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobilePnrSadadServlet"
#define PNR_CONFIRM_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobilePnrConfirmServlet"
#define PNR_CANCEL_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobilePnrCancelServlet"
#define PNR_RETRIEVE_PNR_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobilePnrSrvServlet"
#define TAG_CANCEL 1
#define TAG_SADAD27 2
#define TAG_SADAD_CONFIRMATION 3

@interface BookingDisplayTableViewController ()

@end

@implementation BookingDisplayTableViewController


- (void)viewDidLoad
{
    //Check Internet Connection
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = YES;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skyBgAlpha.png"]];
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    [Utility isInternetReachable];
    
    NSString *title = [NSString stringWithFormat:@"%@ - %@", self.booking.alphanumeric, self.booking.numeric];
    [super viewDidLoad];
    
    self.webView.delegate = self;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.title =title;
    [self deactivateButtons];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:BOOKING_DISPLAY];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(id) init
{
    if (self) {
        
        itn = [[BookingItinerary alloc]init];
        
        NSLog(@"init");
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* title = [[NSString alloc]init];
    
    switch (section) {
        case 1:
            title = @"Itenraries";
            break;
            
        case 2:
            title = @"Passengers";
            break;
            
        case 3:
            title = @"Time Limit";
            break;
        case 4:
            title = @"Tickets";
            break;
    }
    
    return title;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    int rows =0;
    
    switch (section) {
        case 0:
            // Servicing Commands
            rows = 1;
            break;
        case 1:
            // Itineraries
            rows = self.booking.itineraries.count;
            break;
        case 2:
            // Passengers
            rows = self.booking.names.count;
            break;
        case 3:
            // Time Limit
            rows = 1;
            break;
        case 4:
            // Tickets
        {
            if (self.booking.tickets.count == 0) {
                rows = 1;
            }
            else
            {
                rows = self.booking.tickets.count;
            }
            
        }
            break;
    }
    
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float height = 45;
    
    switch (indexPath.section) {
        case 0:
            height = 58;
            break;
        case 1:
            height = 84;
            break;
        case 2:
            height = 45;
            break;
        case 3:
            height = 56;
            break;
        case 4:
            height = 45;
            break;
    }
    
    return height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"bookingServicesCell";
    UITableViewCell *curCell = [[UITableViewCell alloc]init] ;
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        case 0:
            curCell = self.BookingServicesCell;
            break;
            
            
        case 1:
        {
            static NSString *CellIdentifier = @"itenraryCell";
            
            curCell = (ItenraryTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (curCell == nil){
                NSLog(@"New Cell Made");
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ItenraryTableViewCell" owner:self options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[ItenraryTableViewCell class]])
                    {
                        itn = [self.booking.itineraries objectAtIndex:indexPath.row];
                        curCell = (ItenraryTableViewCell *)currentObject;
                        ((ItenraryTableViewCell *)curCell).fromLabel.text = itn.origin;
                        ((ItenraryTableViewCell *)curCell).toLabel.text = itn.dest;
                        ((ItenraryTableViewCell *)curCell).flightNoLabel.text = [NSString stringWithFormat:@"%@%@",itn.carrier, itn.flightNo];
                        ((ItenraryTableViewCell *)curCell).dateLabel.text = itn.depDate;
                        ((ItenraryTableViewCell *)curCell).departureTimeLabel.text = itn.departureTime;
                        ((ItenraryTableViewCell *)curCell).arrivalTimeLabel.text =itn.arrivalTime;
                        ((ItenraryTableViewCell *)curCell).flightStatusLabel.text = itn.status;
                        //titleCell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
            
        }
            break;
            
        case 2:
        {
            static NSString *CellIdentifier = @"passengerCell";
            
            curCell = (PassengerTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (curCell == nil){
                NSLog(@"New Cell Made");
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PassengerTableViewCell" owner:self options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[PassengerTableViewCell class]])
                    {
                        curCell = (PassengerTableViewCell *)currentObject;
                        ((PassengerTableViewCell *)curCell).passengerName.text = [self.booking.names objectAtIndex:indexPath.row];
                        //titleCell.backgroundColor = [UIColor clearColor];
                        break;
                    }
                }
            }
        }
            break;
        case 3:
        {
            if (self.booking.timelimit == nil) {
                self.timeLimitLabel.text = @"No time limit associated for this booking";
            }
            else
            {
                self.timeLimitLabel.text = self.booking.timelimit;
            }
            curCell = self.timeLimitCell;
            
        }
            break;
            
        case 4:
        {
            static NSString *CellIdentifier = @"ticketCell";
            
            if (self.booking.tickets.count == 0) {
                curCell = self.noTicketsCell;
            }
            else
            {
                curCell = (TicketTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (curCell == nil){
                    NSLog(@"New Cell Made");
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TicketTableViewCell" owner:self options:nil];
                    
                    for(id currentObject in topLevelObjects)
                    {
                        if([currentObject isKindOfClass:[TicketTableViewCell class]])
                        {
                            curCell = (TicketTableViewCell *)currentObject;
                            ((TicketTableViewCell*)curCell).ticketLabel.text = [self.booking.tickets objectAtIndex:indexPath.row];
                            //titleCell.backgroundColor = [UIColor clearColor];
                            break;
                        }
                    }
                }
            }
        }
            break;
    }
    
    return curCell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Navigation logic may go here, for example:
 // Create the next view controller.
 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
 
 // Pass the selected object to the new view controller.
 
 // Push the view controller.
 [self.navigationController pushViewController:detailViewController animated:YES];
 }
 */
- (void)UIWebViewWithPost:(UIWebView *)uiWebView url:(NSString *)url params:(NSMutableArray *)params
{
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", url]];
    if([params count] % 2 == 1) { NSLog(@"UIWebViewWithPost error: params don't seem right"); return; }
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >\n", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    [s appendString: @"</input></form></body></html>"];
    NSLog(@"r %@", s);
    [uiWebView loadHTMLString:s baseURL:nil];
}

//Internet conection reachability connection TEST
- (void) viewWillAppear:(BOOL)animated
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Entered did finish load of webview");
   
    ServicingBL *servicingBl = [[ServicingBL alloc]init];
    
    NSString *currentURL = self.webView.request.URL.absoluteString;
    NSLog(@"title:%@",currentURL);
    NSString *jsonString = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
    
    if ([currentURL isEqualToString:@PNR_RETRIEVE_PNR_URL])
    {
        [SVProgressHUD dismiss];
        self.booking = [servicingBl retrieveBookingFromJsonOfString:jsonString];
        
        if ([self.booking.status isEqualToString:@"s"]) {
            
            NSLog(@"numeric:%@", [self.booking.names objectAtIndex:0]);
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.booking.message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self.tableView reloadData];
    }
    
    else if ([currentURL isEqualToString:@ACTIVATE_SADDAD_URL])
    {
        messageResponse = [servicingBl getMessageResponseFromJsonOfString:jsonString];
        [SVProgressHUD dismiss];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:messageResponse.message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    else if ([currentURL isEqualToString:@PNR_CONFIRM_URL])
    {
        messageResponse = [servicingBl getMessageResponseFromJsonOfString:jsonString];
         [SVProgressHUD dismiss];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:messageResponse.message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        if ([messageResponse.status isEqualToString:@"s"]) {
            [SVProgressHUD showWithStatus:@"Loading Reservation...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];

            [self pnrRetrieve];
            [self.tableView reloadData];
        }
        
    }
    
    else if ([currentURL isEqualToString:@PNR_CANCEL_URL])
    {
        messageResponse = [servicingBl getMessageResponseFromJsonOfString:jsonString];
        [SVProgressHUD dismiss];
        //if ([messageResponse.status isEqualToString:@"s"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:messageResponse.message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        //}
        
        if ([messageResponse.status isEqualToString:@"s"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    
}

-(void)deactivateButtons
{
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    //if([Utility isInternetReachable])
    //{
        ServicingBL *servicingBL = [[ServicingBL alloc]init];
        
        if ([servicingBL isAllSegmentConfirmedInItineraries:self.booking.itineraries]) {
            self.confirmBookingButton.enabled = NO;
            self.confirmBookingButton.alpha = 0.5;
        }
        if(self.booking.tickets.count>0)
        {
            self.activateSadadButton.enabled = NO;
            self.activateSadadButton.alpha = 0.5;
        }
   // }
}

#pragma mark IBActions

- (IBAction)confirmBooking:(id)sender {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    if([Utility isInternetReachable])
    {
        GLobal *global = [GLobal sharedManager];
        [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];

        NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                         @"userPRN", global.staff.prn,
                                         @"pnr", self.booking.alphanumeric,
                                         nil];
        
        NSLog(@"pnr:%@ userPRN:%@",self.booking.alphanumeric, global.staff.prn);
        
        [self UIWebViewWithPost:self.webView url:@PNR_CONFIRM_URL params:webViewParams];
        
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"
                                                              action:@"touch"
                                                               label:CONFIRM_BOOKING_BUTTON
                                                               value:nil] build]];
        [tracker set:kGAIScreenName value:nil];
    }
}

- (IBAction)activateSadad:(id)sender {
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    if([Utility isInternetReachable])
    {

        //ARE YOU SURE YOU WANT TO PAY WITH SADAD?
        sadad27AlertView = [[UIAlertView alloc] initWithTitle:@"Activate Sadad"
                                                      message:@"Please make sure that you purchase your tickets within 72 hours through SADAD. Otherwise, your booking will get canceled."
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        sadad27AlertView.tag = TAG_SADAD27;
        
        [sadad27AlertView show];
        
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"
                                                              action:@"touch"
                                                               label:ACTIVATE_SADAD_BUTTON
                                                               value:nil] build]];
        [tracker set:kGAIScreenName value:nil];
        
    }
}

- (IBAction)cancelBooking:(id)sender
{
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    
    if([Utility isInternetReachable])
    {
        cancelAlertView = [[UIAlertView alloc] initWithTitle:@"Cancel Booking" message:@"Are you sure do you want to cancel this booking?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
        cancelAlertView.tag = TAG_CANCEL;
        [cancelAlertView show];
        
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX"
                                                              action:@"touch"
                                                               label:CANCEL_BOOKING_BUTTON
                                                               value:nil] build]];
        [tracker set:kGAIScreenName value:nil];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == TAG_CANCEL) {
        
        if (buttonIndex == 1) {
            
            GLobal *global = [GLobal sharedManager];
            ServicingBL *servicingBl = [[ServicingBL alloc]init];
            NSString *refNumbers = @"";
            
            refNumbers = [servicingBl getRefNumberInItineraries:self.booking.itineraries];
            
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"userPRN", global.staff.prn,
                                             @"pnr", self.booking.alphanumeric,
                                             @"segmentNumbers", refNumbers,
                                             nil];
            
            [self UIWebViewWithPost:self.webView url:@PNR_CANCEL_URL params:webViewParams];
            [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];
        }
        else if (buttonIndex == 0)
        {
            // Cancel button response
            
        }
    }
    
    else if (alertView.tag == TAG_SADAD27)
    {
        if (buttonIndex == 0) {
            sadadConfirmationAlertView = [[UIAlertView alloc] initWithTitle:@"Activate Sadad" message:@"ARE YOU SURE YOU WANT TO PAY WITH SADAD?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            sadadConfirmationAlertView.tag = TAG_SADAD_CONFIRMATION;
            [sadadConfirmationAlertView show];
        }
        
    }
    
    else if (alertView.tag == TAG_SADAD_CONFIRMATION)
    {
        if (buttonIndex == 1) {
            GLobal *global = [GLobal sharedManager];
            
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"userPRN", global.staff.prn,
                                             @"pnr", self.booking.alphanumeric,
                                             nil];
            
            [self UIWebViewWithPost:self.webView url:@ACTIVATE_SADDAD_URL params:webViewParams];
            [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];
        }
    }
}

- (void)pnrRetrieve
{
    GLobal *global = [GLobal sharedManager];

    NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                     @"userPRN", global.staff.prn,
                                     @"pnr", self.booking.alphanumeric,
                                     nil];
    [self UIWebViewWithPost:self.webView url:@PNR_RETRIEVE_PNR_URL params:webViewParams];
}

- (void)handleSadadButton
{
    
}

- (void)hudTapped:(NSNotification *)notification
{
    [self.webView stopLoading];
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}
@end

