//
//  MessageResponse.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 7/1/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageResponse : NSObject
@property (copy,nonatomic) NSString *status;
@property (copy,nonatomic) NSString *message;
@end
