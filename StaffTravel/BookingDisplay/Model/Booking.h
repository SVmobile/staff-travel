//
//  Booking.h
//  StaffTravel
//
//  Created by Mohammad Bahadur on 6/16/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Booking : NSObject
@property (copy,nonatomic) NSString *alphanumeric;
@property (copy,nonatomic) NSString *numeric;
@property (copy,nonatomic) NSString *timelimit;
@property (copy,nonatomic) NSMutableArray *names;
@property (copy,nonatomic) NSMutableArray *itineraries;
@property (copy,nonatomic) NSMutableArray *tickets;
@property (copy,nonatomic) NSString *status;
@property (copy,nonatomic) NSString *message;
@end
