//
//  MainMenuViewController.h
//  Staff Travel
//
//  Created by Mohammad Bahadur on 3/26/14.
//  Copyright (c) 2014 Mohammad Bahadur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaffModel.h"
#import "LoginUIViewController.h"
#import "WebCheckInViewController.h"
#import "ContactUsViewController.h"
#import "FlightInfoTableViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"


#define MAIN_MENU @"Main Menu"

@interface MainMenuViewController : UIViewController <UINavigationBarDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(strong,nonatomic) StaffModel* staff;
@property (strong, nonatomic) IBOutlet UILabel *staffName;



// Buttons
- (IBAction)LogoutButton:(id)sender;
- (IBAction)CheckInButton:(id)sender;
- (IBAction)ContactUsButton:(id)sender;
- (IBAction)FlightInfoButton:(id)sender;
- (IBAction)AvailabilityButton:(id)sender;
- (IBAction)FlightScheduleButon:(id)sender;
- (IBAction)Servicing:(id)sender;


@end
