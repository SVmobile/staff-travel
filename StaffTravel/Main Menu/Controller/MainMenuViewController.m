//
//  MainMenuViewController.m
//  Staff Travel
//
//  Created by Mohammad Bahadur on 3/26/14.
//  Copyright (c) 2014 Mohammad Bahadur. All rights reserved.
//

#import "MainMenuViewController.h"
#import "ContactUsViewController.h"
#import "LoginUIViewController.h"
#import "ServicingTableViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    return YES;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:MAIN_MENU];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:138.0/255.0 green:129.0/255.0 blue:100.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Bangla Sangam MN" size:17.0]}];
    [self.navigationController.navigationBar setTintColor:[UIColor darkTextColor]];
    
    self.staffName.text = [NSString stringWithFormat:@"%@",self.staff.name];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


//Main Menu Buttons

- (IBAction)Servicing:(id)sender
{
    ServicingTableViewController *servicingTableViewController = [[ServicingTableViewController alloc]init];
    [self.navigationController pushViewController:servicingTableViewController animated:YES];
}

- (IBAction)LogoutButton:(id)sender {
    
    //Call the delete Cookies function to safely LOGOUT by A.M.
    [Utility deleteCookies];
    
    AppDelegate *appDelegateObj = [[AppDelegate alloc]init];
    
   // NSMutableDictionary *plistDic = [LoginBL readFromPlist]; //To read before removing the DATA from the plist
    
   [LoginBL removeDataFromPlist];
    
    // NSMutableDictionary *plistDic2 = [LoginBL readFromPlist];//To read after removing the DATA from the plist

    
     if(appDelegateObj.loginMainFlag)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        //Back to login
        LoginUIViewController* loginUIViewController = [[LoginUIViewController alloc] init];
        loginUIViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        //loginUIViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:loginUIViewController animated:YES completion:nil];
    }
  //  NSLog(@"Back to login.. %@ \n-- But after deleting the items the plist is: %@", plistDic, plistDic2);
}

- (IBAction)CheckInButton:(id)sender {
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    if([Utility isInternetReachable])
    {
        WebCheckInViewController *webChkIn = [[WebCheckInViewController alloc] init];
        [self.navigationController pushViewController:webChkIn animated:YES];
        NSLog(@"CheckIn Mansho....");
    }
}

- (IBAction)ContactUsButton:(id)sender {
    ContactUsViewController *ctUs = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    [self.navigationController pushViewController:ctUs animated:YES];
    NSLog(@"ContactUs");
}
- (IBAction)FlightInfoButton:(id)sender
{
    FlightInfoTableViewController *flightInfoTableViewController = [[FlightInfoTableViewController alloc] initWithNibName:@"FlightInfoTableViewController" bundle:nil];
    [self.navigationController pushViewController:flightInfoTableViewController animated:YES];
    NSLog(@"FlightInfoTableViewController");
}
- (IBAction)AvailabilityButton:(id)sender
{
    
}
- (IBAction)FlightScheduleButon:(id)sender
{
    
}


@end
