//
//  FlightInfoBL.h
//  StaffTravel
//
//  Created by Mohammed Alquzi on 7/7/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.

//  @

#import <Foundation/Foundation.h>
//#import "Booking.h"
//#import "BookingItinerary.h"
#import "FlightInfoDetail.h"


#define FLIGHTI_NFO_URL "https://portal.saudiairlines.com.sa/Web/MobilePnrSrv/MobileFlightInfoServlet"

@interface FlightInfoBL : NSObject

- (FlightInfoDetail*) retrieveBookingFromJsonOfString:(NSString*)jsonString;;

@end
