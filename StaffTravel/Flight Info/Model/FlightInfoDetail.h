//
//  FlightInfoDetail.h
//  StaffTravel
//
//  Created by Mohammed Alquzi on 7/16/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.

//  @

#import <Foundation/Foundation.h>

@interface FlightInfoDetail : NSObject

//For Flight Info
@property (copy, nonatomic) NSString *departureTime; //1
@property (copy, nonatomic) NSString *arrivalTime;   //2
@property (copy, nonatomic) NSString *status;
@property (copy, nonatomic) NSString *duration;     //3
@property (copy, nonatomic) NSString *airCraft;     //4
@property (copy, nonatomic) NSString *departure;    //5
@property (copy, nonatomic) NSString *arrivalCity;  //6
@property (copy, nonatomic) NSString *messageStatus;
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *departureDate;

//For Actual Flight Info
@property (copy, nonatomic) NSArray *actualFlightInfo;


@end
