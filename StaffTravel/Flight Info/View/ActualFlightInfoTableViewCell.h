//
//  ActualFlightInfoTableViewCell.h
//  StaffTravel
//
//  Created by Mohammed Alquzi on 11/9/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActualFlightInfoTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *actulFlightInfoTextLabel;

@end
