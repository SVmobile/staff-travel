//
//  FlightDetailsTableViewController.m
//  StaffTravel
//
//  Created by Ali Mansho on 7/9/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "FlightDetailsTableViewController.h"

@interface FlightDetailsTableViewController ()

@end

@implementation FlightDetailsTableViewController

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:FLIGHT_INFO_DETAILS];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = YES;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skyBgAlpha.png"]];
    
    self.navigationItem.title = [@"SV " stringByAppendingString:self.flightNoTitle];
    
    self.flightDateLabel.text = self.flightInfo.departureTime;
    self.arrivalCityLabel.text = self.flightInfo.arrivalCity;
    self.departureLabel.text = self.flightInfo.departure;
    self.airCraftLabel.text = self.flightInfo.airCraft;
    self.arrivalTimeLabel.text = self.flightInfo.arrivalTime;
    self.departureTimeLabel.text = self.flightInfo.departureTime;
    self.durationLabel.text = self.flightInfo.duration;
    self.flightDateLabel.text = self.flightInfo.departureDate;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* title = [[NSString alloc]init];
    
    switch (section) {
        case 0:
            title = @"Scheduled Flight Information";
            break;
        case 1:
            title = @"Actual Flight Information";
            break;
    }
    
    return title;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int rows = 0;
    
    switch (section) {
        case 0:
            rows = 6;
            break;
            
        case 1:
            rows = self.flightInfo.actualFlightInfo.count;
            break;
            
        default:
            break;
    }
    
    return rows;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"FlightInfoCell";
    
    UITableViewCell *curCell = [[UITableViewCell alloc]init];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        case 0:
            //Section-1 Flight Info
            if (indexPath.row == 0) {
                curCell = self.dateCell;
            }
            if (indexPath.row == 1) {
                curCell = self.citiesCell;
            }
            if (indexPath.row == 2) {
                curCell = self.durationCell;
            }
            if (indexPath.row == 3) {
                curCell = self.departureCell;
            }
            if (indexPath.row == 4) {
                curCell = self.arrivalCell;
            }
            if (indexPath.row == 5) {
                curCell = self.aircraftCell;
            }
            break;
            
        case 1:
        {
            static NSString *CellIdentifier = @"ActualFlightInfoCell";
            //NSLog(@"flightInfo.actualFlightInfo.count:%lu",(unsigned long)self.flightInfo.actualFlightInfo.count);
            if (self.flightInfo.actualFlightInfo.count == 0)
            {
                //curCell = self.noTicketsCell;   //Handler, to be filled later
                //NSLog(@"No records!!");
            }
            else
            {
                curCell = (ActualFlightInfoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (curCell == nil){
                    //NSLog(@"New Cell Made");
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ActualFlightInfoTableViewCell" owner:self options:nil];
                    
                    for(id currentObject in topLevelObjects)
                    {
                        if([currentObject isKindOfClass:[ActualFlightInfoTableViewCell class]])
                        {
                            curCell = (ActualFlightInfoTableViewCell *)currentObject;
                            ((ActualFlightInfoTableViewCell*)curCell).actulFlightInfoTextLabel.text = [self.flightInfo.actualFlightInfo objectAtIndex:indexPath.row];
                            //titleCell.backgroundColor = [UIColor clearColor];
                            break;
                        }
                    }
                }
                
            }
        }
            
            break;
    } //6
    return curCell;

    }



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    float height = 45;
    //
    //    if (indexPath.section == 0) {
    //        if (indexPath.row == 0) {
    //            height = 58;
    //        } else if(indexPath.row == 3)
    //        {
    //            //float height = 65;
    //             height = 65;
    //        }
    //        
    //    }
    
    return 55;
}

@end
