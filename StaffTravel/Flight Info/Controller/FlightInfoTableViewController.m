//
//  FlightInfoTableViewController.m
//  StaffTravel
//
//  Created by: Ali M. Mansho on 6/15/14.
// Mobile No. 0554372603
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "FlightInfoTableViewController.h"

@interface FlightInfoTableViewController ()

@end

@implementation FlightInfoTableViewController

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:FLIGHT_INFO_INPUT];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}
- (void)viewDidLoad
{
    
    // A.M.
    // Test the internet connection and alert if there is no connection (Reachability)....
    [Utility isInternetReachable];
    
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-54573002-1"];

    [tracker send:[[[GAIDictionaryBuilder createAppView] set:@"Flight Info"
                                                      forKey:kGAIScreenName] build]];
     */
    [super viewDidLoad];
    
    // Google Analytics
    
    
    flightDate=@"T";
    self.webView.delegate = self;
    self.flightNumberTextField.delegate =self;
    self.carrierTextField.delegate = self;
    self.flightNumberTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if (!self.flightNumberTextField.text.length) {
        self.displayFlightInfoOutletButton.enabled = NO;
        self.displayFlightInfoOutletButton.alpha = 0.5;
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = YES;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skyBgAlpha.png"]];
    
    self.title = @"Flight Information";
    
    [self.flightNumberTextField becomeFirstResponder];
    [self.flightNumberTextField setKeyboardType:UIKeyboardTypeNumberPad];
    
    // [self.displayFlightInfoOutletButton setEnabled:NO];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"servicingCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    //  return cell;
    
    //&&&&&
    
    
    static NSString *CellIdentifier = @"servicingCell";
    UITableViewCell *curCell = [[UITableViewCell alloc]init] ;
    if (indexPath.section == 0) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if (indexPath.row == 0) {
            curCell = self.flightNumberCell;
        }
        
        if (indexPath.row == 1) {
            curCell = self.departureDateCell;
        }
        if (indexPath.row == 2) {
            curCell = self.flightInfoDisplayCell;
        }
        if (indexPath.row == 3) {
            curCell = self.carrierCell;
        }
    }
    return curCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    float height = 45;
    //
    //    if (indexPath.section == 0) {
    //        if (indexPath.row == 0) {
    //            height = 58;
    //        } else if(indexPath.row == 3)
    //        {
    //            //float height = 65;
    //             height = 65;
    //        }
    //
    //    }
    
    return 55;
}

- (IBAction)departureDateSwitchbtn:(id)sender
{
    if (self.departureDateSwitchOutlet.on)
    {
        //  NSLog(@"Switch Button is On...");
        self.todayYesterdayLabel.text = @"Today";
        flightDate = @"T";
    }
    else
    {
        // NSLog(@"Switch Button is Off...");
        self.todayYesterdayLabel.text = @"Yesterday";
        flightDate = @"Y";
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    
    // inforece 8 didit numbers code
    NSUInteger newLength = [self.flightNumberTextField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    // The following code is to disable/enable the button if the textfield is empty - It also dims the button when the textfield is empty... The button becomes to be enabled when the user starts typing (Ali Mansho).
    
    NSString *testString = [self.flightNumberTextField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(testString.length)
    {
        self.displayFlightInfoOutletButton.alpha = 1;
        self.displayFlightInfoOutletButton.enabled = YES;
    }
    else if (!testString.length)
    {
        self.displayFlightInfoOutletButton.alpha = .5;
        self.displayFlightInfoOutletButton.enabled = NO;
    }
    // A.M.
    
    return (([string isEqualToString:filtered])&&(newLength <= FLIGHTNUMBER_LIMIT));
}


- (BOOL) textFieldShouldClear:(UITextField *)textField
{
    // The following code is to disable/enable the button if the textfield is empty - It also dims the button when the textfield is empty... The button becomes to be enabled when the user starts typing (Ali Mansho).
    
    self.displayFlightInfoOutletButton.alpha = .5;
    self.displayFlightInfoOutletButton.enabled = NO;
    // A.M.
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([Utility isInternetReachable])
    {
        if (self.flightNumberTextField.text.length == 0)
        {
            self.displayFlightInfoOutletButton.alpha = .5;
            self.displayFlightInfoOutletButton.enabled = NO;
        }
        
        else
        {
            [self.view endEditing:YES];
            FlightDetailsTableViewController *flightDetailsTableViewController = [[FlightDetailsTableViewController alloc] initWithNibName:@"FlightDetailsTableViewController" bundle:nil];
            flightDetailsTableViewController.flightNoTitle = self.flightNumberTextField.text;
            [self.navigationController pushViewController:flightDetailsTableViewController animated:YES];
            
            NSLog(@"FlightDetailsTableViewController");
        }
    }
    return YES;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{[self.view endEditing:YES];}


- (void)UIWebViewWithPost:(UIWebView *)uiWebView url:(NSString *)url params:(NSMutableArray *)params
{
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", url]];
    if([params count] % 2 == 1) { NSLog(@"UIWebViewWithPost error: params don't seem right"); return; }
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >\n", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    [s appendString: @"</input></form></body></html>"];
    NSLog(@"r %@", s);
    [uiWebView loadHTMLString:s baseURL:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hudTapped:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
    
    [SVProgressHUD showWithStatus:@"Loading...\n \n <Tap to Cancel>" maskType:SVProgressHUDMaskTypeGradient];
}

//Internet conection reachability
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    FlightInfoBL *flightInfoBL = [[FlightInfoBL alloc]init];
    FlightInfoDetail *flightInfoDetail = [[FlightInfoDetail alloc]init];
    
    NSString *currentURL = self.webView.request.URL.absoluteString;
    NSLog(@"title:%@",currentURL);
    //NSString *jsonString = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
    // NSLog(@"Json String:%@", jsonString);
    
    
    if ([currentURL isEqualToString:@FLIGHTI_NFO_URL])
    {
        NSString *jsonString = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        NSLog(@"Json String:%@", jsonString);
        flightInfoDetail = [flightInfoBL retrieveBookingFromJsonOfString:jsonString];
        
        NSLog(@"message:%@",flightInfoDetail.messageStatus);
        
        if ([flightInfoDetail.messageStatus isEqualToString:@"s"]) {
            
            
            FlightDetailsTableViewController *flightDetailsTableViewController = [[FlightDetailsTableViewController alloc] initWithNibName:@"FlightDetailsTableViewController" bundle:nil];
            flightDetailsTableViewController.flightNoTitle = self.flightNumberTextField.text;
            flightDetailsTableViewController.flightInfo = flightInfoDetail;
            [self.navigationController pushViewController:flightDetailsTableViewController animated:YES];
            
            
            //NSLog(@"numeric:%@", [flightInfo.names objectAtIndex:0]);
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:flightInfo.message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [SVProgressHUD dismiss];
    }
    
    
}

#pragma mark IBActions
-(IBAction)DisplayFlightInfo:(id)sender
{
    // GLobal *global = [GLobal sharedManager];
    [self.view endEditing:YES];
    if ([Utility isInternetReachable])
    {
        NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                         @"fltDate", flightDate,
                                         @"carrier", @"SV",
                                         @"fltNo", self.flightNumberTextField.text,
                                         nil];
        NSLog(@"flight_info");
        
        [self UIWebViewWithPost:self.webView url:@FLIGHTI_NFO_URL params:webViewParams];
    }
}

//This method is to cancel the laoding when a user taps on it by Ali Mansho (A.M.)
- (void)hudTapped:(NSNotification *)notification
{
    [self.webView stopLoading];
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}

@end
