//
//  FlightDetailsTableViewController.h
//  StaffTravel
//
//  Created by Ali Mansho on 7/9/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlightInfoTableViewController.h"
#import "FlightInfoDetail.h"
#import "FlightInfoBL.h"
#import "ActualFlightInfoTableViewCell.h"
//#import "FlightInfo.h"

#define FLIGHT_INFO_DETAILS @"Flight Info Details"

@interface FlightDetailsTableViewController : UITableViewController


@property (strong, nonatomic) IBOutlet UITableViewCell *dateCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *citiesCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *departureCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *arrivalCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *durationCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *aircraftCell;

@property (strong, nonatomic)  NSString *flightNoTitle;
@property (strong ,nonatomic) FlightInfoDetail* flightInfo;

@property (strong, nonatomic) IBOutlet UILabel *flightDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *arrivalCityLabel;
@property (strong, nonatomic) IBOutlet UILabel *departureLabel;
@property (strong, nonatomic) IBOutlet UILabel *airCraftLabel;
@property (strong, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *departureTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;




@end
