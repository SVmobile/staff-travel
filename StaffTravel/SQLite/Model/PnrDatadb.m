//
//  PnrDatadb.m
//  StaffTravel
//
//  Created by Ali Mansho on 6/12/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "PnrDatadb.h"

@implementation PnrDatadb

static PnrDatadb *instance=nil;
-(id) init
{
    self = [super initWithFilename:@"pnrdb.sqlite" deleteEditableCopy:FALSE];//FALSE means that if you find the Database, don't delete it, if we type YES, that means evertime you open the app, the Database should be deleted and then created it again.
    if(self)
    {
        if(sqlite3_open([_writableDBPath UTF8String], &database) == SQLITE_OK) //So UTF8String converts the NString (in Objective-C) to c string (Character String for C language). & means a referece, means double pointers, a reference for the Database.
        {
            [self initializeStatement: &insertPnr withSQL:"INSERT INTO PNRDATA(pnrC, prnC, flight_dateC, originC, destinationC, jsonC) VALUES (?,?,?,?,?,?)"];
            [self initializeStatement: &updatePnr withSQL:"UPDATE PNRDATA SET prnC = ?, flight_dateC = ?, originC =?, destinationC = ?, jsonC = ? WHERE pnrC =?"];
            [self initializeStatement: &deletePnr withSQL:"DELETE FROM PNRDATA WHERE pnrC=?"];
            [self initializeStatement: &selectPnr withSQL:"SELECT * FROM PNRDATA"];
            [self initializeStatement: &selectPnrWithPnr withSQL:"SELECT * FROM PNRDATA WHERE pnrC = ?"];
            
            [self initializeStatement: &selectPnrWithPrn withSQL:"SELECT * FROM PNRDATA WHERE prnC = ?"];
            
          //  NSLog(@"selectPnrWithPnr => %@", selectPnrWithPnr);
        }
    }
    return self;
}


-(BOOL) insertIntoPnr:(NSString *)pnrC  prn:(NSString *)prnC flightDate:(NSString *)flight_dateC origin:(NSString *)originC destination:(NSString *)destinationC json:(NSString *)jsonC{
    
    sqlite3_bind_text(insertPnr, 1, [pnrC UTF8String], -1, SQLITE_TRANSIENT); //1 is for the index which means the first ? in SQL.
    sqlite3_bind_text(insertPnr, 2, [prnC UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertPnr, 3, [flight_dateC UTF8String], -1, SQLITE_TRANSIENT); //UTF8String is to convert form objective-c to C.
    sqlite3_bind_text(insertPnr, 4, [originC UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertPnr, 5, [destinationC UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertPnr, 6, [jsonC UTF8String], -1, SQLITE_TRANSIENT);
    
    return [self executeUpdate:insertPnr];
}
-(BOOL) updatePnr:(NSString *)pnrC prn:(NSString *)prnC flightDate:(NSString *)flight_dateC origin:(NSString *)originC destination:(NSString *)destinationC json:(NSString *)jsonC

{
    sqlite3_bind_text(updatePnr, 6,[pnrC UTF8String], -1, SQLITE_TRANSIENT); //5 is for the index which means the second ? in SQL.
    sqlite3_bind_text(updatePnr, 1, [prnC UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updatePnr, 2, [flight_dateC UTF8String], -1, SQLITE_TRANSIENT); //UTF8String is to convert form objective-c to C.
    sqlite3_bind_text(updatePnr, 3, [originC UTF8String], -1, SQLITE_TRANSIENT);//UTF8String is to convert form objective-c to C.
    sqlite3_bind_text(updatePnr, 4, [destinationC UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updatePnr, 5, [jsonC UTF8String], -1, SQLITE_TRANSIENT);

    return [self executeUpdate:updatePnr];
}
-(BOOL) deletePnr:(NSString *)pnrC
{
    sqlite3_bind_text(deletePnr, 1, [pnrC UTF8String], -1, SQLITE_TRANSIENT); //1 is for the index which means the first ? in SQL.
    return [self executeUpdate:deletePnr];
}
-(NSArray *) selectFromPnr
{
    NSMutableArray *pnrs = [[NSMutableArray alloc]init];
    while ([self executeSelect:selectPnr]) {
        PnrData *pnrData =[[PnrData alloc]init];
        char * pnr = (char *) sqlite3_column_text(selectPnr, 0);
        pnrData.pnrC = [NSString stringWithUTF8String:pnr];
        
        char *prn = (char *) sqlite3_column_text(selectPnr, 1);
        pnrData.prnC = [NSString stringWithUTF8String:prn];
        char *flightDate = (char *) sqlite3_column_text(selectPnr, 2);
        pnrData.flight_dateC = [NSString stringWithUTF8String:flightDate];
        char *origin = (char *) sqlite3_column_text(selectPnr, 3);
        pnrData.originC = [NSString stringWithUTF8String:origin];
        char *destination = (char *) sqlite3_column_text(selectPnr, 4);
        pnrData.destinationC = [NSString stringWithUTF8String:destination];
        char *json = (char *) sqlite3_column_text(selectPnr, 5);
        pnrData.jsonC = [NSString stringWithUTF8String:json];

        [pnrs addObject:pnrData];
    }
    sqlite3_reset(selectPnr);
    return pnrs;
}

-(NSMutableArray *) selectFromPnrDataWithPnr:(NSString *)pnrC
{
    //NSLog(@"pnrC  (inside the method..) ==> %@", pnrC);

    sqlite3_bind_text(selectPnrWithPnr, 1, [pnrC UTF8String], -1,SQLITE_TRANSIENT); //1 is for the index which means the first ? in SQL.
   // NSLog(@"Statement is: %@", selectPnrWithPnr);
    NSMutableArray *pnrs = [[NSMutableArray alloc]init];
    while ([self executeSelect:selectPnrWithPnr]) {
        PnrData *pnrData =[[PnrData alloc]init];
        char * pnr = (char *) sqlite3_column_text(selectPnrWithPnr, 0);
        pnrData.pnrC = [NSString stringWithUTF8String:pnr];
        char *prn = (char *) sqlite3_column_text(selectPnrWithPnr, 1);
        pnrData.prnC = [NSString stringWithUTF8String:prn];

        char *flightDate = (char *) sqlite3_column_text(selectPnrWithPnr, 2);
        pnrData.flight_dateC = [NSString stringWithUTF8String:flightDate];
        char *origin = (char *) sqlite3_column_text(selectPnrWithPnr, 3);
        pnrData.originC = [NSString stringWithUTF8String:origin];
        char *destination = (char *) sqlite3_column_text(selectPnrWithPnr, 4);
        pnrData.destinationC = [NSString stringWithUTF8String:destination];
        char *json = (char *) sqlite3_column_text(selectPnrWithPnr, 5);
        pnrData.jsonC = [NSString stringWithUTF8String:json];
        
        [pnrs addObject:pnrData];
    }
    sqlite3_reset(selectPnrWithPnr);
    return pnrs;
}


-(NSMutableArray *) selectFromPnrDataWithPrn:(NSString *)prnC
{
    NSLog(@"prnC  (inside the method..) ==> %@", prnC);
    
    sqlite3_bind_text(selectPnrWithPrn, 1, [prnC UTF8String], -1,SQLITE_TRANSIENT); //1 is for the index which means the first ? in SQL.
    // NSLog(@"Statement is: %@", selectPnrWithPnr);
    NSMutableArray *pnrs = [[NSMutableArray alloc]init];
    while ([self executeSelect:selectPnrWithPrn]) {
        PnrData *pnrData =[[PnrData alloc]init];
        char * pnr = (char *) sqlite3_column_text(selectPnrWithPrn, 0);
        pnrData.pnrC = [NSString stringWithUTF8String:pnr];
        
        char *prn = (char *) sqlite3_column_text(selectPnrWithPrn, 1);
        pnrData.prnC = [NSString stringWithUTF8String:prn];
        char *flightDate = (char *) sqlite3_column_text(selectPnrWithPrn, 2);
        pnrData.flight_dateC = [NSString stringWithUTF8String:flightDate];
        char *origin = (char *) sqlite3_column_text(selectPnrWithPrn, 3);
        pnrData.originC = [NSString stringWithUTF8String:origin];
        
        char *destination = (char *) sqlite3_column_text(selectPnrWithPrn, 4);
        pnrData.destinationC = [NSString stringWithUTF8String:destination];
        char *json = (char *) sqlite3_column_text(selectPnrWithPrn, 5);
        pnrData.jsonC = [NSString stringWithUTF8String:json];

        [pnrs addObject:pnrData];
    }
    sqlite3_reset(selectPnrWithPrn);
    
    //By Ali Mansho
    //The following line is to reverse the array as to display the last PNR the user searches for.... (A.M.)
    pnrs = [[pnrs reverseObjectEnumerator] allObjects];
    return pnrs;
}

+(PnrDatadb *) sharedInstance
{
    if(instance == nil)
    {
        instance = [[PnrDatadb alloc] init];
    }
    return instance;
}

+(id) alloc
{
    NSAssert (instance == nil, @"Attempt to create second instance");
    instance = [super alloc];
    return instance;
}

// To remove the whole queries from the memory...
-(void) dealloc
{
    if(insertPnr != nil)
    {
        sqlite3_reset(insertPnr);
        sqlite3_finalize(insertPnr);
        insertPnr = nil;
    }
    
    if(updatePnr != nil)
    {
        sqlite3_reset(updatePnr);
        sqlite3_finalize(updatePnr);
        updatePnr = nil;
    }
    
    if(deletePnr != nil)
    {
        sqlite3_reset(deletePnr);
        sqlite3_finalize(deletePnr);
        deletePnr = nil;
    }
    
    if(selectPnr != nil)
    {
        sqlite3_reset(selectPnr);
        sqlite3_finalize(selectPnr);
        selectPnr = nil;
    }
    
    if(selectPnrWithPnr != nil)
    {
        sqlite3_reset(selectPnrWithPnr);
        sqlite3_finalize(selectPnrWithPnr);
        selectPnrWithPnr = nil;
    }
    
    if(selectPnrWithPrn != nil)
    {
        sqlite3_reset(selectPnrWithPrn);
        sqlite3_finalize(selectPnrWithPrn);
        selectPnrWithPrn = nil;
    }
}
@end
