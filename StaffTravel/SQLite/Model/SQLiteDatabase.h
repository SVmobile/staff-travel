//
//  SQLiteDatabase.h
//  StaffTravel
//
//  Created by Ali Mansho on 6/12/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SQLiteDatabase : NSObject
{
	NSString *_writableDBPath;
	
	sqlite3 *database;
}



/* Initializers
 * - deleteEditableCopy: Set to TRUE to clear out any previous editable copy and start fresh.
 */
-(id)initWithFilename:(NSString *)filename deleteEditableCopy:(BOOL)deleteCopy;

-(BOOL)initializeStatement:(sqlite3_stmt **)statement withSQL:(const char*)sql;


- (void)createEditableCopyOfDatabaseIfNeeded:(NSString*)databaseFilename sourcePath:(NSString*)sourcePath deleteEditableCopy:(BOOL)deleteCopy;

- (BOOL)executeUpdate:(sqlite3_stmt *)statement;
- (BOOL)executeSelect:(sqlite3_stmt *)statement;
- (BOOL)executeCount:(sqlite3_stmt *)statement;
- (BOOL)executeStatement:(sqlite3_stmt *)statement success:(int)successConstant;
- (int)executeintQuery:(sqlite3_stmt *)statement;

@end
