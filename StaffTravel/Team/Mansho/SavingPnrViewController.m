//
//  SavingPnrViewController.m
//  StaffTravel
//
//  Created by Ali Mansho on 6/15/14.
//  Copyright (c) 2014 SaudiaMobileDevelopmentTeam. All rights reserved.
//

#import "SavingPnrViewController.h"

@interface SavingPnrViewController ()

@end

@implementation SavingPnrViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.pnrTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.dateTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.originTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.destinationTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.jsonTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchPnrTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.prnTextField.clearButtonMode = UITextFieldViewModeWhileEditing; 
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)retrievePnrDataButton:(id)sender
{
    PnrDatadb *tempdb = [PnrDatadb sharedInstance];
    NSArray *pnrs;
    PnrData *pnr;
    
    if (self.searchPnrTextField.text.length == 0 )
    {
        //To display all the data from the DB.
        pnrs = [tempdb selectFromPnr];
    }
   
    else if (self.prnTextField.text.length > 0 )
    {
        //To display all the data from the DB.
        pnrs = [tempdb selectFromPnrDataWithPrn:self.prnTextField.text];
    }
    
    else
    {
        // To display data for a specific PNR from the DB
        pnrs = [tempdb selectFromPnrDataWithPnr:self.searchPnrTextField.text];
    }
    
    // NSLog(@"before loop...! %@", pnrs);
   // NSLog(@"Your crateria...! %@", self.pnrTextField.text);
    
    if ([pnrs count] == 0)
    {
        UIAlertView *noDataAlert = [[UIAlertView alloc] initWithTitle:@"There is no data to show." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [noDataAlert show];
    }
    else
    {
        
        NSLog(@"There are [%lu] PNRs", (unsigned long)[pnrs count]);
    
        for(pnr in pnrs)
        {
            NSLog(@"%@ , %@, %@, %@, %@", pnr.pnrC, pnr.flight_dateC, pnr.originC, pnr.destinationC, pnr.jsonC);
            
            self.pnrTextField.text = pnr.pnrC;
            self.prnTextField.text = pnr.prnC; 
            self.dateTextField.text = pnr.flight_dateC;
            self.originTextField.text = pnr.originC;
            self.destinationTextField.text = pnr.destinationC;
            self.jsonTextField.text = pnr.jsonC;
        }
    }
}

- (IBAction)savePnrDataButton:(id)sender
{
    PnrDatadb *tempdb = [PnrDatadb sharedInstance];
    NSArray *pnrs = [tempdb selectFromPnrDataWithPnr:self.pnrTextField.text];
    
    NSString *errorMessage = nil;
    BOOL isErrorOccured = FALSE;
    if(self.pnrTextField.text.length == 0)
    {
        errorMessage = @"PNR can not be empty.";
        isErrorOccured = TRUE;
    }
    else if(self.dateTextField.text.length == 0)
    {
        errorMessage =@"Flight date can not be empty.";
        isErrorOccured = TRUE;
    }
    else if(self.originTextField.text.length == 0)
    {
        errorMessage =@"Origin can not be empty.";
        isErrorOccured = TRUE;
    }
    else if(self.destinationTextField.text.length == 0)
    {
        errorMessage =@"Destination can not be empty.";
        isErrorOccured = TRUE;
    }
    else if(self.jsonTextField.text.length == 0)
    {
        errorMessage =@"JSON data can not be empty.";
        isErrorOccured = TRUE;
    }
    
    else if(self.prnTextField.text.length == 0)
    {
        errorMessage =@"PRN can not be empty.";
        isErrorOccured = TRUE;
    }
    
    else if([pnrs count] != 0)
    {
        errorMessage =@"Sorry, the PNR is already existed, you can NOT add it again.";
        isErrorOccured = TRUE;
    }
    if(isErrorOccured)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorMessage message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }

    else
    {
        GLobal *globa = [GLobal sharedManager];
        [tempdb insertIntoPnr: self.pnrTextField.text  prn:globa.staff.prn flightDate:self.dateTextField.text origin:self.originTextField.text destination:self.destinationTextField.text json:self.jsonTextField.text];
    
    pnrs = [tempdb selectFromPnr];
    for(PnrData *pnr in pnrs)
    {
        NSLog(@"%@ , %@, %@, %@, %@, %@", pnr.pnrC, pnr.flight_dateC, pnr.originC, pnr.destinationC, pnr.jsonC, pnr.prnC);
    }
        self.pnrTextField.text = @"";
        self.prnTextField.text = @"";
        self.dateTextField.text = @"";
        self.originTextField.text = @"";
        self.destinationTextField.text = @"";
        self.jsonTextField.text = @"";
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Your data has been saved." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert2 show];
    }
}


- (IBAction)updatePnrDataButton:(id)sender
{
    PnrDatadb *tempdb;
    tempdb = [PnrDatadb sharedInstance];
    NSArray *pnrs = [tempdb selectFromPnrDataWithPnr:self.searchPnrTextField.text];
    
    if ([pnrs count] == 0)
    {
        UIAlertView *noDataToDeleteAlert = [[UIAlertView alloc] initWithTitle:@"There is no data to update." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [noDataToDeleteAlert show];
    }
    else
    {
        [tempdb updatePnr:self.pnrTextField.text prn:self.prnTextField.text flightDate:self.dateTextField.text origin:self.originTextField.text destination:self.destinationTextField.text json:self.jsonTextField.text];
        UIAlertView *updateAlert = [[UIAlertView alloc] initWithTitle:@"Your PNR has been updated." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [updateAlert show];
    }
}


- (IBAction)deletePnrDataButton:(id)sender
{
    PnrDatadb *tempdb;
    tempdb = [PnrDatadb sharedInstance];
    NSArray *pnrs = [tempdb selectFromPnrDataWithPnr:self.searchPnrTextField.text];
    
    if ([pnrs count] == 0)
    {
        UIAlertView *noDataToDeleteAlert = [[UIAlertView alloc] initWithTitle:@"There is no data to delete." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [noDataToDeleteAlert show];
    }
    else
    {
        [tempdb deletePnr: self.searchPnrTextField.text];
        UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:@"Your PNR has been deleted." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [deleteAlert show];
    }
}




-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)logoutButton:(id)sender {
    //Call deleteCookies function to delete the cookies
    [Utility deleteCookies];
    
    //Back to login
    LoginUIViewController *loginUIViewController = [[LoginUIViewController alloc] initWithNibName:@"LoginUIViewController" bundle:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Back to login..");
    
}

- (IBAction)clearFields:(id)sender
{
    self.pnrTextField.text = @"";
    self.prnTextField.text = @"";
    self.dateTextField.text = @"";
    self.originTextField.text = @"";
    self.destinationTextField.text = @"";
    self.jsonTextField.text = @"";
}


@end
